function openNav() {
  document.getElementById("my-nav").style.height = "100%";
}

function closeNav() {
  document.getElementById("my-nav").style.height = "0%";
}

document.onkeyup = function(e){
  if( e.keyCode === 27){
    closeNav();
  }
}
