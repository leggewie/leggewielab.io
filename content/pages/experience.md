Title: experience
Slug: experience
Index: 3

Purpose of this site
--------------------

This site showcases some of my skills as applicable in a DevOps environment.  To
that end, I'm using some of the tools and follow the practices of the trade as [I
learned them]({filename}ressources.md) even if they are entirely oversized for the task at hand.

The code for this website is publicly available in a [version-controlled
repository](https://gitlab.com/leggewie/leggewie.gitlab.io/).  Go ahead and
click on the button in the top right to fork it in case you like what you see.
The code passes a few simple automated checks before being automatically
deployed from a [CI/CD pipeline](https://gitlab.com/leggewie/leggewie.gitlab.io/-/blob/master/.gitlab-ci.yml).

I am by no means a web developer or a graphics designer.  I kindly ask that you
don't judge me too harshly in that regard.

Skills
------
- decades of experience working remotely and in distributed FOSS teams
- highly proficient in Linux administration including headless and remote
    - desktop, server and embedded
    - mail server Postfix
    - MySQL/MariaDB including Galera Cluster
    - web server apache2 and nginx
    - django webframework
    - ...
- highly proficient in packaging software for Debian, Ubuntu and derivatives
- highly proficient in version-control systems, especially git
- highly proficient in bug-tracking and QA
- I am quadrilingual (German, English, Japanese, French)
- advanced knowledge of python
- good knowledge of bash scripting
- good knowledge of RegEx
- good knowledge of ansible
- good knowledge of gitlab CI
- good knowledge of docker, portainer and traefik
- self-starter and fast self-learner

Philosophy
----------
- I live and breathe the command-line, including remote ssh, screen and tmux sessions
- I love [good docs]({filename}ressources.md)
- I believe in self-hosting and owning your data
- I believe in the importance of learning and an open mind
- I believe in sharing, respectful communication and meritocracy
- I believe in "release early and release often"
- I believe in incremental improvements (kaizen/改善)
- I believe in automation and carving out repeatable tasks
- I believe in the importance of attention to detail and ultimately aiming for perfection

Certificates & Awards
---------------------

- first place on the [June 2021 leaderboard](https://py.checkio.org/profile/leaderboard/all/2021/6/) at Python CheckIO
- Linkedin [Succeeding in DevOps]({static}/pdf/RolfLeggewie_Linkedin_cert_succedding_in_devops.pdf)
- Linkedin [Learning Docker]({static}/pdf/CertificateOfCompletion_Learning_Docker.pdf)
- IBM [Data Analysis with Python](https://courses.cognitiveclass.ai/certificates/77b15a3eb74a4f34a0e45ddc3b33112a)
- IBM [Python for Data Science](https://courses.cognitiveclass.ai/certificates/a586a6061da9419aa79668d65c4cc997)
- HackerRank [Python Basics](https://www.hackerrank.com/certificates/7656487cb668)

Shut up and show me the code
----------------------------

My contributions and snippets are still scattered around a number of places. I am
in the process of making them more centrally available.

- [code](https://gitlab.com/leggewie/leggewie.gitlab.io/), [content](https://gitlab.com/leggewie/leggewie.gitlab.io/-/tree/master/content) and [CI/CD](https://gitlab.com/leggewie/leggewie.gitlab.io/-/blob/master/.gitlab-ci.yml) for this site
- portfolio [HelloWorld project](https://gitlab.com/leggewie/flask-helloworld/) to build, test and securely deploy a dockerized HelloWorld webapp in an [automated pipeline](https://gitlab.com/leggewie/flask-helloworld/-/blob/master/.gitlab-ci.yml)
- [checkio.org](https://py.checkio.org/user/leggewie/solutions/share/556fe5b5fb8082815ab0814f293d15e4/) - a great community for learning python and TypeScript
- [HackerRank](https://www.hackerrank.com/leggewie) - requires login to view individual solutions
- [my humble contributions](https://galaxy.ansible.com/leggewie/) to Ansible Galaxy
