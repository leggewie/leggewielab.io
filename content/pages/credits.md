Title: credits
Slug: credits
Index: 9
Nofollow: True

Credits and Licenses
--------------------

* [Adam Tindale](http://www.adamtindale.com) for the inspiration to some of the layout and pelican theme
* [PresentationGo](https://www.presentationgo.com/presentation/infographic-triangle-for-powerpoint/) for the base of my DevOps triangle image


[comment]: <> (https://mmenujs.com/mburger/)
