Title: home
Url:
Save_as: index.html
Slug: index.html
Index: 1

Rolf Leggewie
=============

Objective: apply my expertise in Python, QA and Linux in an SRE or DevOps role
------------------------------------------------------------------------------

I have decades of [experience]({filename}experience.md) with Linux administration and QA/bug triage. Combined with my Python skills and keen ability to dive into any new area fast, that forms a solid foundation for any kind of DevOps or SRE assignment.

![DevOps as a triangle - QA identifies and communicates issues, Ops fixes issues and DevOps prevents issues](images/devops-triangle-grey.png "Rolfs skills form a solid foundation for DevOps work")

I am currently based in Germany, but willing to work remotely for assignments worldwide.
