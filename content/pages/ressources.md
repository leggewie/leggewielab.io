Title: ressources
Slug: ressources
Index: 5

Ressources
==========

bits and pieces that I found useful

Python
------

- [CheckIO](https://py.checkio.org/) - fun, gamified and peer-reviewed python coding exercises
- [Python docs](https://docs.python.org/3/) - official reference just a click away
- [Dive Into Python 3](https://diveintopython3.net) by Mark Pilgrim
- [Giraffe Academy Python](https://www.youtube.com/watch?v=rfscVS0vtbw)
- [CS50@Harvard](https://www.youtube.com/watch?v=hnDU1G9hWqU)
- [Zen of Python](https://en.wikipedia.org/wiki/Zen_of_Python)
- ["Stop writing classes"](https://www.youtube.com/watch?v=o9pEzgHorH0) by Jack Diederich

[//]: # (CI/CD, Unit Testing and Github activity)

RegEx
-----

- [Python RegEx@W3 Schools](https://www.w3schools.com/python/python_regex.asp)
- [RegEx101](https://regex101.com)

Devops
------

- [DevOps history](https://www.youtube.com/watch?v=o7-IuYS0iSE) by Damon Edwards
- [Look Ma' no Kubernetes](https://www.youtube.com/watch?v=ZnZJXI377ak) by Alex Ellis - low-overhead serverless function-as-a-service without K8s or AWS Lambda
- [Why OpenStack?](https://www.youtube.com/watch?v=9-br7qDuuK4) (private cloud IaC)
- [Install OpenStack locally](https://ubuntu.com/openstack/install)
- [deploy with ArgoCD](https://www.youtube.com/watch?v=MeU5_k9ssrs) by Techworld with Nana
- [Python for DevOps](https://www.youtube.com/watch?v=dbCBe7hlLbk) by Noah Gift
- [Ansible for DevOps](https://www.ansiblefordevops.com) by Jeff Geerling
- [Pets vs Cattle](http://cloudscaling.com/blog/cloud-computing/the-history-of-pets-vs-cattle/) by Randy Bias
- [DevOps and SRE](https://www.ibm.com/cloud/blog/three-differences-between-devops-and-sre) by Erika LeBris

Development best practices
--------------------------
- [12-factor app](https://12factor.net)
- [Semantic Versioning](https://semver.org/)
- [Conventional Commits](https://www.conventionalcommits.org/)
- [DRY](https://en.wikipedia.org/wiki/Don't_repeat_yourself)
- [Yagni](https://martinfowler.com/bliki/Yagni.html)

Logging
-------

- [Introduction to Prometheus](https://www.youtube.com/watch?v=h4Sl21AKiDg) by Nana Janashia

Ansible
-------

- [Ansible docs](https://docs.ansible.com/ansible/latest/index.html)
- [Ansible for DevOps](https://www.ansiblefordevops.com) by Jeff Geerling
- [Ansible Templates](https://www.youtube.com/watch?v=tcP_gxOo7mk) by TopTechSkills

Agile
=====

- [How to explain Agile and Scrum to your Grandpa in 5 minutes](https://medium.com/swlh/how-to-explain-agile-and-scrum-to-your-grandpa-in-5-mins-and-to-better-understand-both-yourself-3792748fa085)
