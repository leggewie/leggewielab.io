#!/usr/bin/env python
# -*- coding: utf-8 -*- #

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
OUTPUT_PATH = 'public'
PATH = 'content'
THEME = 'themes/simple-animation'

AUTHOR = 'Rolf Leggewie'
AUTHOR_URL = False
AUTHOR_SAVE_AS = False
AUTHORS_URL = False
AUTHORS_SAVE_AS = False

SITENAME = 'Rolf Leggewie @DevOps'
#SITEURL = 'http://portfolio.leggewie.org'
SITESUBTITLE = ''

# MENUITEMS = (
#     ('Home', ''),
# #     ('Bio/CV/Events', 'info'),
# #     ('Projects', 'projects'),
#     ('Ressources', 'ressources'),
# #     ('Code', 'code'),
#     )
DISPLAY_PAGES_ON_MENU = True
PAGE_ORDER_BY = 'index'

PATH = 'content'
THEME = 'themes/simple-animation'

TIMEZONE = 'Europe/Paris'
DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (
     ('linkedin', 'https://www.linkedin.com/in/RolfLeggewie/'),
     ('checkio', 'https://py.checkio.org/user/leggewie/solutions/share/556fe5b5fb8082815ab0814f293d15e4/'),
     ('gitlab', 'https://gitlab.com/leggewie'),
     ('github', 'https://github.com/leggewie'),
     ('stackoverflow', 'https://askubuntu.com/users/889771/leggewie'),
#     ('stackoverflow', 'https://stackoverflow.com/users/10468807/leggewie'),
# stackshare
     ('email', '&#109;&#097;&#105;&#108;&#116;&#111;:&#106;&#111;&#098;&#115;&#064;&#050;&#049;&#046;&#108;&#101;&#103;&#103;&#101;&#119;&#105;&#101;&#046;&#111;&#114;&#103;'),
     )
GITHUB_URL = 'https://github.com/leggewie/'

DEFAULT_PAGINATION = False

# Disable unneeded blog features
ARCHIVES_SAVE_AS = ""
CATEGORY_SAVE_AS = ""
CATEGORIES_SAVE_AS = ""
TAGS_SAVE_AS = ""
TAG_SAVE_AS = False
TAG_URL = False

# ### XXX ###
#
# ARTICLE_PATHS = ['blog']
# ARTICLE_URL = 'blog/{category}/{slug}/'
# ARTICLE_SAVE_AS = 'blog/{category}/{slug}/index.html'
#
# PDF_GENERATOR = False
#
# PAGE_PATHS = ['pages']
# PAGE_URL = '{slug}/'
# PAGE_SAVE_AS = '{slug}/index.html'
#
# INDEX_SAVE_AS = 'blog/index.html'
#
# # http://stackful-dev.com/static-site-jinja-pelican-shared-templates.html
#
# DEFAULT_DATE_FORMAT = '%A %B %d, %Y'
#
# EXTRA_PATH_METADATA = {
#         'js/jquery.github-activity.js' : {'path': 'theme/js/jquery.github-activity.js'},
#         'js/jquery.knob.js' : {'path': 'theme/js/jquery.knob.js'},
#         'js/jquery.timeago.js' : {'path': 'theme/js/jquery.timeago.js'},
#         'js/prism.js' : {'path': 'theme/js/prism.js'},
#         'css/prism.js' : {'path': 'theme/css/prism.css'},
#         'js/flocking-all.min.js' : {'path': 'theme/js/flocking-all.min.js'},
#         'records/record.html' : {'path': 'record/index.html'},
#         'records/simple.html' : {'path': 'simple/index.html'},
#         'records/oscillate.html' : {'path': 'oscillate/index.html'},
#         'records/sphere.html' : {'path': 'sphere/index.html'},
#         }
#
# PLUGIN_PATHS = ['pelican-plugins']
# #PLUGINS = ['neighbors', 'code_include', 'pelican_gist', 'gist_directive', 'pelican_git']
# PLUGINS = ['neighbors', 'code_include',  'gist_directive']
#
# GIT_CACHE_ENABLED = True
#
# SITEMAP = {
#         'format': 'xml',
#         'changefreqs': {
#             'articles': 'monthly',
#             'indexes': 'daily',
#             'pages': 'monthly'
#             }
#         }
#
#
# LOAD_CONTENT_CACHE = False
